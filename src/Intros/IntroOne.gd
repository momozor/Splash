extends Control

func on_continue_pressed():
    get_tree().change_scene("res://src/CharacterSetup.tscn")

func _ready():
    get_node("Continue").connect("pressed", self, "on_continue_pressed")
