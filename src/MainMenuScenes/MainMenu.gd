extends Control

func on_start_pressed():
    get_tree().change_scene("res://src/Intros/IntroOne.tscn")

func on_quit_pressed():
    get_tree().quit()

func _ready():
    # Place the children of VBoxContainer in the center
    # of viewport
    var viewport_center_x = float(get_viewport().size.x / 2)
    var viewport_center_y = float(get_viewport().size.y / 2)

    get_node("VBoxContainer").set_position(Vector2(viewport_center_x, viewport_center_y))

    get_node("VBoxContainer/Start").connect("pressed", self, "on_start_pressed")
    get_node("VBoxContainer/Quit").connect("pressed", self, "on_quit_pressed")
