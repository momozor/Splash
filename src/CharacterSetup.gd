extends Control

func on_continue_pressed():
    get_tree().change_scene("res://src/CharacterBackgroundOne.tscn")

func _ready():

    # Gender choice
    get_node("DoubleGrid/RightVBox/GenderChoice").add_item("Male", -1)
    get_node("DoubleGrid/RightVBox/GenderChoice").add_item("Female", 0)
    get_node("DoubleGrid/RightVBox/GenderChoice").add_item("Rather not say", 1)

    # Faction choice
    get_node("DoubleGrid/RightVBox/FactionChoice").add_item("British", -1)
    get_node("DoubleGrid/RightVBox/FactionChoice").add_item("Spain", 0)

    # Vessel choice
    get_node("DoubleGrid/RightVBox/VesselChoice").add_item("Sloop", -1)
    get_node("DoubleGrid/RightVBox/VesselChoice").add_item("Brig", 0)

    get_node("Continue").connect("pressed", self, "on_continue_pressed")
