# Splash
Splash is a graphical text based video game about piracy including the
politics, economics and history (some are fictionalized) surrounding it
across the globe.

## Disclaimer
This video is not aiming to be totally historically accurate.

## Contributing

### Programming
This project uses Godot 3.2, OpenGL 2.x and GDScript for development.
Please see TODO file of ideas that are yet to be implemented.

### Art/Media
Currently as I'm writing this, audio and visual arts are not yet needed
but it WILL be soon.

### Documentation
The wiki page should be the place about the game, but not yet due to
very early status of development.

### Support Me!
<a href="https://www.patreon.com/bePatron?u=30235218" data-patreon-widget-type="become-patron-button">Become a Patron!</a>
and support me to finish this project!

## License
This project is released under the AGPL-3.0 license. Please
see LICENSE file for more details.
